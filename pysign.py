#Get all the pictures of the folder and convert them to signed ones
import os
from PIL import Image
import subprocess
import time

def get_pics() :
    result = []
    pic_list = os.listdir(".")
    for pic in pic_list :
        is_pic = True
        #We test if it is a file
        if not os.path.isfile(pic) :
            is_pic = False
        #We test if it is a jpg
        ext = pic.split('.')[-1]
        if ext != 'jpg' and ext != 'JPG' :
            #print pic+" : not jpg"
            is_pic = False
        #We test if it is already signed
        if pic[0:5] == "sign_" :
            is_pic = False
        #We append it to the result list
        if is_pic :
            result.append(pic)
    return result

#exifs infos
orientation_key = 274 #cf exifs tags
rotate_values = {
    3: 180,
    6: 270,
    8: 90
}


def main () :
    pic_list = get_pics()
    overlay = Image.open('sig.png','r')
    sig_width,sig_height = overlay.size
    #Try to create a subfolder
    if not os.path.exists("sig_versions") :
        os.makedirs("sig_versions")
    for pic in pic_list :
        source_file = Image.open(pic,'r')
        #We will try to detect if rotated
        exif = source_file._getexif()
        if exif :
                if orientation_key in exif :
                    orientation = exif[orientation_key]
                    #print pic+" : rotated "+str(exif[orientation_key])
                    if orientation in rotate_values :
                        source_file=source_file.rotate(rotate_values[orientation])
        width,height = source_file.size
        offset = width-(sig_width+30),height-(sig_height+30)
        #source_file.show()
        source_file.paste(overlay, offset, overlay)
        #Saving the new image
        source_file.save('sig_versions/sign_'+pic,'JPEG')

main()
